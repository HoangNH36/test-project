import React from 'react'
import { Pagination } from 'semantic-ui-react'

const PaginationComponent = (props) => {
  let totalPage = 1
  let textSearch
  if (props.trucks) {
    totalPage = Math.ceil(props.countRecord/10);
  }
  if (props.textSearch) {
    textSearch = props.textSearch
  }
  return (
      <Pagination
        className="pagination"
        defaultActivePage={1}
        totalPages={totalPage}
        onPageChange={(e, { activePage }) => { 
          if(textSearch){
            props.changePage(textSearch, activePage) 
          }else{
            props.changePage(activePage) 
          }
        }}
      />
  )
}

export default PaginationComponent