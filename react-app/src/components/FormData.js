import React, { Component } from 'react';
import { Button, Form, Dropdown } from 'semantic-ui-react'
import './styles/FormData.css'

class FormData extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      parkingAddress: props.dataChoose.parkingAddress,
      description: props.dataChoose.description,
      truckPlate: props.dataChoose.truckPlate,
      cargoType: props.dataChoose.cargoType,
      driver: props.dataChoose.driver,
      truckType: props.dataChoose.truckType,
      price: props.dataChoose.price,
      dimensionH: props.dataChoose.dimension.h,
      dimensionL: props.dataChoose.dimension.l,
      dimensionW: props.dataChoose.dimension.w,
      status: props.dataChoose.status,
      productionYear: props.dataChoose.productionYear,
      statusList:[{ key: 1, value: "In-use", text: "In-use" }, { key: 2, value: "New", text: "New" }, { key: 3, value: "Stopped", text: "Stopped" }]

    };
  }

  validateTruckPlate(truckPlate){
    const userKeyRegExp = /^[0-9]{2}[A-Z]-[0-9]{5}?$/
    const valid = userKeyRegExp.test(truckPlate);

    return valid
  }

  validate(formData){
    if(!formData.truckPlate || !formData.status || !formData.price || !formData.cargoType || !formData.driver){
      alert('Please Fill In Required Field !')
      return false
    }else if(!this.validateTruckPlate(formData.truckPlate)){
      alert('Please Follow The Format 12A-12345 !')
      return false
    }else if(formData.cargoType.length > 10){
      alert('Please Do Not Choose Cargo Type More Than 10 !')
      return false
    }else if(formData.parkingAddress.length >500){
      alert('The Parking Address Max Characters Is 500 !')
      return false
    }else if(formData.description.length >200){
      alert('The Description Max Characters Is 200 !')
      return false
    }else{
      return true
    }
  }

  handleSave(){
    let id
    if(this.props.dataChoose.id){
      id = this.props.dataChoose.id
    }
    let formData = {
      "parkingAddress": this.state.parkingAddress,
      "description": this.state.description,
      "truckPlate": this.state.truckPlate,
      "cargoType": this.state.cargoType,
      "driver": this.state.driver,
      "truckType": this.state.truckType,
      "price":this.state.price,
      "dimension":{
        "l":this.state.dimensionL,
        "w":this.state.dimensionW,
        "h":this.state.dimensionH
      },
      "status":this.state.status,
      "productionYear":this.state.productionYear
    }
    
    if(this.validate(formData)){
      if(id){
        this.props.update(formData, id)
        alert('Update Successfully !')
      }else{
        this.props.save(formData)
        alert('Save Successfully !')
      }
    }
  }

  render(){
    let parkingAddressColor, descriptionColor
    let {parkingAddress, description, truckPlate, cargoType, driver,
    truckType, price, dimensionH, dimensionL, dimensionW, status, productionYear} = {...this.state}
    let cargoList = []
    if(this.props){
      if(this.props.cargoType){
        this.props.cargoType.map((item, key) =>{
          return cargoList = [...cargoList,{ key: key, value: item.name, text: item.name }]
        })
      }
    }
    if(this.state.parkingAddress && this.state.parkingAddress.length > 500){
      parkingAddressColor = "red"
    }else{
      parkingAddressColor = "black"
    }
    if(this.state.description && this.state.description.length > 200){
      descriptionColor = "red"
    }else{
      descriptionColor = "black"
    }
    return(
      <Form autoComplete="on">
        <Form.Field>
          <label>Truck plate <span className="required">*</span></label>
          <input placeholder='Truck plate' value={truckPlate} autoComplete="off" onChange={(e)=>{this.setState({truckPlate: e.target.value})}}/>
        </Form.Field>
        <Form.Field>
          <label>Cargo type<span className="required">*</span></label>
          <Dropdown value={cargoType} placeholder='Cargo type' fluid multiple search selection options={cargoList} onChange={(e, {value}) => {this.setState({cargoType: value})}}/>
        </Form.Field>
        
        <Form.Group widths='equal'>
          <Form.Field>
            <label>Driver</label>
            <input placeholder='Driver' value={driver} autoComplete="off" onChange={(e)=>{this.setState({driver: e.target.value})}}/>
          </Form.Field>
          <Form.Field>
            <label>Truck type</label>
            <input placeholder='Truck type' autoComplete="off" value={truckType} onChange={(e)=>{this.setState({truckType: e.target.value})}}/>
          </Form.Field>
          <Form.Field>
            <label>Price<span className="required">*</span></label>
            <input placeholder='Price' autoComplete="off" value={price} onChange={(e)=>{this.setState({price: e.target.value})}}/>
          </Form.Field>
        </Form.Group>
    
        <Form.Group widths='equal'>
          <Form.Field>
            <label>Dimension (L)</label>
            <input placeholder='Dimension (L)' autoComplete="off" value={dimensionL} onChange={(e)=>{this.setState({dimensionL: e.target.value})}}/>
          </Form.Field>
          <Form.Field>
            <label>Dimension (W)</label>
            <input placeholder='Dimension (W)' autoComplete="off" value={dimensionW} onChange={(e)=>{this.setState({dimensionW: e.target.value})}}/>
          </Form.Field>
          <Form.Field>
            <label>Dimension (H)</label>
            <input placeholder='Dimension (H)' autoComplete="off" value={dimensionH} onChange={(e)=>{this.setState({dimensionH: e.target.value})}}/>
          </Form.Field>
        </Form.Group>
    
        <Form.Field>
          <label>Parking address</label>
          <input placeholder='Parking address' autoComplete="off" value={parkingAddress} onChange={(e)=>{this.setState({parkingAddress: e.target.value})}}/>
          <p style={{color: parkingAddressColor}}>{this.state.parkingAddress?this.state.parkingAddress.length:0}/500</p>
        </Form.Field>
    
        <Form.Group widths='equal'>
          <Form.Field>
            <label>Production year</label>
            <input placeholder='Production year' autoComplete="off" value={productionYear} onChange={(e)=>{this.setState({productionYear: e.target.value})}}/>
          </Form.Field>
          <Form.Field>
            <label>Status<span className="required">*</span></label>
            <Dropdown value={status} placeholder='Cargo type' fluid selection options={this.state.statusList} onChange={(e, {value}) => {this.setState({status: value})}}/>
          </Form.Field>
        </Form.Group>
    
        <Form.Field>
          <label>Description</label>
          <input placeholder='Description' autoComplete="off" value={description} onChange={(e)=>{this.setState({description: e.target.value})}}/>
          <p style={{color: descriptionColor}}>{this.state.description?this.state.description.length:0}/200</p>
        </Form.Field>
        <Button type='submit' color='green' onClick={() => {this.handleSave()}}>Save</Button>
      </Form>
    )}
  }
  

export default FormData