import React, { Component } from 'react';
import { Dropdown, Button } from 'semantic-ui-react'

class Sort extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            field: '',
            type:'',
            fieldOptions: [ 
                { key: '1', value: 'truckPlate', text: 'Truck plate' },
                { key: '2', value: 'cargoType', text: 'Cargo type' },
                { key: '3', value: 'driver', text: 'Driver' },
                { key: '4', value: 'truckType', text: 'Truck type' },
                { key: '5', value: 'price', text: 'Price' },
                { key: '6', value: 'parkingAddress', text: 'Parking address' },
                { key: '7', value: 'productionYear', text: 'Production year' },
                { key: '8', value: 'status', text: 'Status' },
                { key: '9', value: 'description', text: 'Description' }
            ],
            typeOptions: [ 
                { key: '1', value: 'asc', text: 'asc' },
                { key: '2', value: 'desc', text: 'desc' }
            ]
        };
      }

    handleClick(){
        this.props.sortByField(this.state.field, this.state.type)
    }

    render() {
        return (
            <div style={{float: "right"}}>
                <Dropdown placeholder='Field' search selection options={this.state.fieldOptions} onChange={(e, {value})=>{this.setState({field: value})}}/>
                <Dropdown placeholder='asc/desc' search selection options={this.state.typeOptions} onChange={(e, {value})=>{this.setState({type: value})}}/>
                <Button color='olive' onClick={() => {this.handleClick()}}>Sort</Button>
            </div>
        )
    }
}


export default Sort
