import React, { Component } from 'react';
import history from '.././history'
import { Input, Button } from 'semantic-ui-react'

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputState: '',
    };
  }

  handleClick(){
    this.props.search(this.state.inputState)
    history.push('/search-result')
  }

  render() {
    return (
      <div>
        <Input placeholder='Search Here...' onChange={(e) => {this.setState({inputState: e.target.value})}}/>
        <Button color='blue' onClick={() => this.handleClick()}>Search Now</Button>
      </div>
    );
  }
}

export default SearchBox;