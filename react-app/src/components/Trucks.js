import React, { Component } from 'react';
import { Table, Confirm } from 'semantic-ui-react'
import history from '../history'

class Trucks extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            open: false,
            id: 0
        };
      }
    handleClick(rowData){
        this.props.chooseInTable(rowData)
        this.setState({open: true, id: rowData.id})
    }

    handleConfirm(){
        history.push('/update')
        this.setState({open: false})
    }

    handleCancel(){
        this.setState({open: false})
        this.props.delete(this.state.id)
    }

    render() {
        let trucks = this.props.trucks
        let listData
        if(trucks){
            listData = trucks.map((item, key) => {
                let listCargo = []
                let count = 1
                item.cargoType.map((cargo) =>{
                   if(count===item.cargoType.length){
                    listCargo.push(cargo)
                   }else{
                    listCargo.push(cargo + ', ')
                   }
                   return count++
                })
                return(
                    <Table.Row key={key} onClick={()=>{this.handleClick(item)}}>
                        <Table.Cell>{item.truckPlate}</Table.Cell>
                        <Table.Cell>{listCargo}</Table.Cell>
                        <Table.Cell>{item.driver}</Table.Cell>
                        <Table.Cell>{item.truckType} ton</Table.Cell>
                        <Table.Cell>{item.price}</Table.Cell>
                        <Table.Cell>{item.dimension.l}-{item.dimension.w}-{item.dimension.h}</Table.Cell>
                        <Table.Cell>{item.parkingAddress}</Table.Cell>
                        <Table.Cell>{item.productionYear}</Table.Cell>
                        <Table.Cell>{item.status}</Table.Cell>
                        <Table.Cell>{item.description}</Table.Cell>
                    </Table.Row>
                )
            })
        }
        return (
            <div>
                
                <Table celled style={{marginTop: 30}}>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Truck plate</Table.HeaderCell>
                        <Table.HeaderCell>Cargo type</Table.HeaderCell>
                        <Table.HeaderCell>Driver</Table.HeaderCell>
                        <Table.HeaderCell>Truck type</Table.HeaderCell>
                        <Table.HeaderCell>Price</Table.HeaderCell>
                        <Table.HeaderCell>Dimension (L-W-H)</Table.HeaderCell>
                        <Table.HeaderCell>Parking address</Table.HeaderCell>
                        <Table.HeaderCell>Production year</Table.HeaderCell>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell>Description</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>

                    <Table.Body>
                      {listData}
                    </Table.Body>
                </Table>

                <Confirm
                  open={this.state.open}
                  cancelButton='Delete'
                  confirmButton="Update"
                  content='Please Choose [Delete] Or [Update]!'
                  onCancel={()=>this.handleCancel()}
                  onConfirm={()=>this.handleConfirm()}
                />
                
            </div>
        );
    }
}

export default Trucks;