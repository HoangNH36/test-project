import React, { Component } from 'react';

class Logo extends Component {
  render() {
    return (
        <div>
            <a href="/">
                <img style={{
                  width: "100%",
                  height: 400
                }} src="/images/mack_hero_cards_desktop2.png" alt="logo" />
            </a>
            <h1 style={{
                marginLeft: 600
            }}>Truck Company - <a href="https://www.facebook.com/hoang.ngo.54" >Contact Us</a>
            </h1>
            
        </div>
      
    )
  }
}

export default Logo;