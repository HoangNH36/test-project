import React from 'react'
import { Button } from 'semantic-ui-react'
import history from '.././history'

const ButtonAdd = () => (
  <div style={{float: "right"}}>
    <Button color='green' onClick={() => {history.push('/create-new')}}>Add new</Button>
  </div>
)

export default ButtonAdd