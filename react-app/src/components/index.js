import Search from './Search'
import Footer from './Footer'
import Trucks from './Trucks'
import Logo from './Logo'
import Pagination from './Pagination'
import AddButton from './AddButton'
import FormData from './FormData'
import Sort from './Sort'

export {
    Search,
    Footer,
    Trucks,
    Logo,
    Pagination,
    AddButton,
    FormData,
    Sort
}