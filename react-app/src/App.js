import React, { Component } from 'react';
import * as container from './containers/index';
import Routes from './routes';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <container.HeaderContainer/>
        <Routes />
        <container.FooterContainer />
      </div>
    );
  }
}

export default App;
