import React, { Component } from 'react';
import * as components from '.././components/index'
import {connect} from 'react-redux'
import * as action from '../actions/Actions'

class HeaderContainer extends Component {
  render() {
    return (
        <div className="header">
            <div className="logo">
              <components.Logo />
            </div>
            <components.Search {...this.props} />
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      textSearch: state.trucks.textSearch
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      search : (textSearch) => {
          dispatch(action.search(textSearch, 1))
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(HeaderContainer)