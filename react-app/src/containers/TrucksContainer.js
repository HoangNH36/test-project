import React, { Component } from 'react';
import * as components from '.././components/index'
import {connect} from 'react-redux'
import * as action from '../actions/Actions'
class TrucksContainer extends Component {

componentDidMount(){
    this.props.initLoad()
    this.props.getTotalCount()
}
  render() {
    return (
        <div className="trucklist">
            <components.Trucks {...this.props} />
            <div style={{marginLeft: 400, marginTop: 30}}>
                <components.Pagination {...this.props} />
                <components.AddButton {...this.props} />
                <components.Sort {...this.props} />
            </div>
        </div>
      
    );
  }
}
const mapStateToProps = (state) => {
    return {
        trucks : state.trucks.listTrucks,
        textSearch: state.trucks.textSearch,
        countRecord: state.trucks.countRecord
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        initLoad : () => {
            dispatch(action.getListTrucks(1))
        },
        getTotalCount : () => {
            dispatch(action.getTotalCount())
        },
        changePage: (activePage) => {
            dispatch(action.getListTrucks(activePage))
        },
        chooseInTable: (rowData) => {
            dispatch(action.chooseInTable(rowData))
        },
        delete: (id) => {
            dispatch(action.deleteTruck(id))
        },
        sortByField:(field, type) => {
            dispatch(action.sortByField(field, type))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TrucksContainer)