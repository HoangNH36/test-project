import React, { Component } from 'react';
import * as components from '../components'
import {connect} from 'react-redux'
import * as action from '../actions/Actions'

class FormCreateContainer extends Component {
componentDidMount(){
    this.props.initLoad()
}
  render() {
    return (
        <div className="formCreate">
            <components.FormData {...this.props} />
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      cargoType : state.cargos.listCargo,
      dataChoose: {
        parkingAddress: '',
        description: '',
        truckPlate: '',
        cargoType: [],
        driver: '',
        truckType: '',
        price:'',
        dimension:{
          l:'',
          w:'',
          h:''
        },
        status:'',
        productionYear:''
      }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      initLoad: () => {
        dispatch(action.getListCargo())
      },
      save : (data) => {
        dispatch(action.save(data))
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(FormCreateContainer)