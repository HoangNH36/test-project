import React, { Component } from 'react';
import * as components from '.././components/index'
import { connect } from 'react-redux'
import * as action from '../actions/Actions'
class ResultContainer extends Component {
    componentDidMount() {
        this.props.getTotalRecord()
    }
    render() {
        return (
            <div className="result">
                <components.Trucks {...this.props} />
                <div style={{ marginLeft: 400, marginTop: 30 }}>
                    <components.Pagination {...this.props} />
                    <components.AddButton {...this.props} />
                    <components.Sort {...this.props} />
                </div>
            </div>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        trucks: state.trucks.listTrucks,
        textSearch: state.trucks.textSearch,
        countRecord: state.trucks.countRecord
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        changePage: (textSearch, activePage) => {
            dispatch(action.search(textSearch, activePage))
        },
        chooseInTable: (rowData) => {
            dispatch(action.chooseInTable(rowData))
        },
        delete: (id) => {
            dispatch(action.deleteTruck(id))
        },
        sortByField: (field, type) => {
            dispatch(action.sortByField(field, type))
        },
        searchAll: (textSearch) => {
            console.log("textSearch:", textSearch);

            dispatch(action.getTotalCountSearch(textSearch))
        }
    }
}

const mergeProps = (stateProps, dispatchProps) => {
    return {
        ...stateProps,
        ...dispatchProps,
        getTotalRecord: () => (
            dispatchProps.searchAll(
                stateProps.textSearch
            )
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ResultContainer)