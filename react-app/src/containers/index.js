import HeaderContainer from './HeaderContainer'
import FooterContainer from './FooterContainer'
import TrucksContainer from './TrucksContainer'
import ResultContainer from './ResultContainer'
import FormCreateContainer from './FormCreateContainer'
import FormUpdateContainer from './FormUpdateContainer'

export {
    HeaderContainer,
    FooterContainer,
    TrucksContainer,
    ResultContainer,
    FormCreateContainer,
    FormUpdateContainer
}