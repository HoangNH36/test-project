import React, { Component } from 'react';
import * as components from '../components'
import {connect} from 'react-redux'
import * as action from '../actions/Actions'

class FormUpdateContainer extends Component {
componentDidMount(){
    this.props.initLoad()
}
  render() {
    return (
        <div className="formUpdate">
            <components.FormData {...this.props} />
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      cargoType : state.cargos.listCargo,
      dataChoose : state.trucks.dataChoose
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      initLoad: () => {
        dispatch(action.getListCargo())
      },
      update : (data, id) => {
        dispatch(action.update(data, id))
      }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(FormUpdateContainer)