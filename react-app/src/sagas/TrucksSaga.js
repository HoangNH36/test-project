import { put, takeEvery } from 'redux-saga/effects'
import getListTrucks from '../fetchAPI/getTrucks'
import saveTruck from '../fetchAPI/saveTruck'
import updateTruck from '../fetchAPI/updateTruck'
import deleteTruck from '../fetchAPI/deleteTruck'
import sortDataByField from '../fetchAPI/sortDataByField'
import getAllTrucks from '../fetchAPI/getAllTrucks'

function* getTrucks(action) {
    try {
        const listTrucks = yield getListTrucks(action.payload)
        yield put({
            type: 'GET_TRUCKS_SUCCESS',
            payload: listTrucks
        })
    } catch (error) {
        yield put({
            type: 'GET_TRUCKS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* getAllTruckData(action) {
    try {
        const listTrucks = yield getAllTrucks(action.payload)
        yield put({
            type: 'GET_ALL_TRUCKS_SUCCESS',
            payload: listTrucks
        })
    } catch (error) {
        yield put({
            type: 'GET_ALL_TRUCKS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* save(action) {
    try {
        const truck = yield saveTruck(action.payload)
        yield put({
            type: 'SAVE_SUCCESS',
            payload: truck
        })
    } catch (error) {
        yield put({
            type: 'SAVE_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* update(action) {
    try {
        const truck = yield updateTruck(action.payload.data, action.payload.id)
        yield put({
            type: 'UPDATE_SUCCESS',
            payload: truck
        })
    } catch (error) {
        yield put({
            type: 'UPDATE_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* deleteData(action) {
    try {
        yield deleteTruck(action.payload)
        yield put({
            type: 'DELETE_SUCCESS',
            payload: action.payload
        })
    } catch (error) {
        yield put({
            type: 'DELETE_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* sortData(action) {
    try {
        const listTrucks = yield sortDataByField(action.payload.field, action.payload.type)
        yield put({
            type: 'GET_TRUCKS_SUCCESS',
            payload: listTrucks
        })
    } catch (error) {
        yield put({
            type: 'GET_TRUCKS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}


export const TrucksSaga = [
    takeEvery('GET_TRUCKS_REQUEST', getTrucks),
    takeEvery('GET_ALL_TRUCKS_REQUEST', getAllTruckData),
    takeEvery('SAVE_REQUEST', save),
    takeEvery('UPDATE_REQUEST', update),
    takeEvery('DELETE_REQUEST', deleteData),
    takeEvery('SORT_REQUEST', sortData)
];
