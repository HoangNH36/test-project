import { all } from 'redux-saga/effects'
import {TrucksSaga} from './TrucksSaga'
import {SearchSaga} from './SearchSaga'
import {CargosSaga} from './CargosSaga'

function* rootSaga() {
	yield all([
		...TrucksSaga,
		...SearchSaga,
		...CargosSaga
	]);
}
export default rootSaga;
