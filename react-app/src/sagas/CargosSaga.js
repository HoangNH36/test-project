import { put, takeEvery } from 'redux-saga/effects'
import getCargos from '../fetchAPI/getCargos'

function* getListCargo(action) {
    try {
        const listCargo = yield getCargos()
        yield put({
            type: 'GET_CARGOS_SUCCESS',
            payload: listCargo
        })
    } catch (error) {
        yield put({
            type: 'GET_CARGOS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}


export const CargosSaga = [
    takeEvery('GET_CARGOS_REQUEST', getListCargo),
];
