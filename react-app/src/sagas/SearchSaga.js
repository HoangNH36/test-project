import { put, takeEvery } from 'redux-saga/effects'
import search from '../fetchAPI/search'
import searchAll from '../fetchAPI/searchAll'

function* searchData(action) {
    try {
        const listTrucks = yield search(action.payload.textSearch, action.payload.page)
        yield put({
            type: 'GET_TRUCKS_SUCCESS',
            payload: listTrucks
        })
    } catch (error) {
        yield put({
            type: 'GET_TRUCKS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* searchAllData(action) {
    try {
        const listTrucks = yield searchAll(action.payload)
        console.log("input :",action.payload.textSearch);
        
        console.log("data :",listTrucks);
        console.log("length :",listTrucks.length);
        yield put({
            type: 'SEARCH_ALL_TRUCKS_SUCCESS',
            payload: listTrucks
        })
    } catch (error) {
        yield put({
            type: 'SEARCH_ALL_TRUCKS_FAILURE',
            payload: {
                errorMessage: error.message
            }
        })
    }
}

export const SearchSaga = [
    takeEvery('SEARCH', searchData),
    takeEvery('SEARCH_ALL_TRUCKS_REQUEST', searchAllData)
];
