export default function saveTruck(data, id) {

    return new Promise((resolve, reject) => {
      
      const url = `http://localhost:3001/trucks/${id}`
      fetch(url, {
        method: "PUT",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  
  