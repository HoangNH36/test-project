export default function sortDataByField(field, type) {

    return new Promise((resolve, reject) => {
      
      const url = `http://localhost:3001/trucks?_page=1&_limit=10&_sort=${field}&_order=${type}`
      fetch(url, {
        method: "GET"
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  
  