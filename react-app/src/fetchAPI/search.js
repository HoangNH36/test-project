export default function search(textSearch, page) {

    return new Promise((resolve, reject) => {
      
      const url = `http://localhost:3001/trucks?q=${textSearch}&_page=${page}&_limit=10`
      fetch(url, {
        method: "GET"
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }


  
  