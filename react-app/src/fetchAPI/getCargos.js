export default function getCargos() {

    return new Promise((resolve, reject) => {
      
      const url = 'http://localhost:3001/cargo'
      fetch(url, {
        method: "GET"
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  
  