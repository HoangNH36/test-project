import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import * as pages from './pages/index'
import history from './history';

const Routes = () => (
    <Router history = {history}>
        <Switch>
            <Route exact path="/" component={pages.HomePage} />
            <Route path="/search-result" component={pages.ResultPage} />
            <Route path="/create-new" component={pages.CreatePage} />
            <Route path="/update" component={pages.UpdatePage} />
        </Switch>
    </Router>
);
export default Routes
