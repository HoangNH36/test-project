const DEFAULT_STATE = {
    listTrucks : null,
    dataFetched: false,
    isFetching: false,
    error: false,
    errorMessage: null,
    textSearch: null,
    isSaving: false,
    isSaved: false,
    dataChoose: null,
    isUpdating: false,
    isUpdated: false,
    isDeleting: false,
    isDeleted: false,
    countRecord: 0
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case 'GET_TRUCKS_REQUEST':
            return {
                ...state,
                isFetching: true,
            }
        case 'GET_TRUCKS_SUCCESS':
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage:null,
                listTrucks: action.payload
            }
        case 'GET_TRUCKS_FAILURE':
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }
        case 'GET_ALL_TRUCKS_REQUEST':
            return {
                ...state,
                isFetching: true,
            }
        case 'GET_ALL_TRUCKS_SUCCESS':
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage:null,
                countRecord: action.payload.length
            }
        case 'GET_ALL_TRUCKS_FAILURE':
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }
        case 'SEARCH_ALL_TRUCKS_REQUEST':
            return {
                ...state,
                isFetching: true,
            }
        case 'SEARCH_ALL_TRUCKS_SUCCESS':
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage:null,
                countRecord: action.payload.length
            }
        case 'SEARCH_ALL_TRUCKS_FAILURE':
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }
        case 'SEARCH':
            return {
                ...state,
                isFetching: true,
                textSearch: action.payload.textSearch
            }    
        case 'SAVE_REQUEST':
            return {
                ...state,
                error: false,
                errorMessage:null,
                isSaving: true
            }
        case 'SAVE_SUCCESS':
            return {
                ...state,
                error: false,
                errorMessage:null,
                isSaving: false,
                isSaved: true
            }
        case 'SAVE_FAILURE':
            return {
                ...state,
                error: true,
                errorMessage: action.payload.errorMessage,
                isSaving: false,
                isSaved: false
            }
        case 'UPDATE_REQUEST':
            return {
                ...state,
                error: false,
                errorMessage:null,
                isUpdating: true
            }
        case 'UPDATE_SUCCESS':
            return {
                ...state,
                error: false,
                errorMessage:null,
                isUpdating: false,
                isUpdated: true
            }
        case 'UPDATE_FAILURE':
            return {
                ...state,
                error: true,
                errorMessage: action.payload.errorMessage,
                isUpdating: false,
                isUpdated: false
            }
        case 'CHOOSE_IN_TABLE':
            return {
                ...state,
                dataChoose: action.payload
            }
        case 'DELETE_REQUEST':
            return {
                ...state,
                error: false,
                errorMessage:null,
                isDeleting: true
            }
        case 'DELETE_SUCCESS':
            let newListTrucks = state.listTrucks.filter((item)=>{return item.id!==action.payload})
            return {
                ...state,
                error: false,
                errorMessage:null,
                isDeleting: false,
                isDeleted: true,
                listTrucks: newListTrucks
            }
        case 'DELETE_FAILURE':
            return {
                ...state,
                error: true,
                errorMessage: action.payload.errorMessage,
                isDeleting: false,
                isDeleted: false
            }
        default:
            return state;
    }

}
