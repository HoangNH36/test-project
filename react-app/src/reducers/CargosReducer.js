const DEFAULT_STATE = {
    listCargo : null,
    dataFetched: false,
    isFetching: false,
    error: false,
    errorMessage: null
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case 'GET_CARGOS_REQUEST':
            return {
                ...state,
                isFetching: true,
            }
        case 'GET_CARGOS_SUCCESS':
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage:null,
                listCargo: action.payload
            }
        case 'GET_CARGOS_FAILURE':
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }
        default:
            return state;
    }

}
