import { combineReducers } from 'redux'
import CargosReducer from './CargosReducer'
import TrucksReducer from './TrucksReducer'

export default combineReducers({
    trucks: TrucksReducer,
    cargos: CargosReducer
})
