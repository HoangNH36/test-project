export function getListTrucks(payload){
    return ({
        type: "GET_TRUCKS_REQUEST",
        payload
    })
}

export function search(textSearch, page){
    return ({
        type: "SEARCH",
        payload: {
            textSearch,
            page
        }
    })
}

export function save(payload){
    return ({
        type: "SAVE_REQUEST",
        payload
    })
}

export function update(data, id){
    return ({
        type: "UPDATE_REQUEST",
        payload: {
            data,
            id
        }
    })
}

export function getListCargo(payload){
    return ({
        type: "GET_CARGOS_REQUEST",
        payload
    })
}

export function chooseInTable(payload){
    return ({
        type: "CHOOSE_IN_TABLE",
        payload
    })
}

export function deleteTruck(payload){
    return ({
        type: "DELETE_REQUEST",
        payload
    })
}

export function sortByField(field, type){
    return ({
        type: "SORT_REQUEST",
        payload: {
            field,
            type
        }
    })
}

export function getTotalCount(payload){
    return ({
        type: "GET_ALL_TRUCKS_REQUEST",
        payload
    })
}

export function getTotalCountSearch(payload){
    return ({
        type: "SEARCH_ALL_TRUCKS_REQUEST",
        payload
    })
}