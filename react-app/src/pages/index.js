import HomePage from './HomePage'
import ResultPage from './ResultPage'
import CreatePage from './CreatePage'
import UpdatePage from './UpdatePage'

export {
    HomePage,
    ResultPage,
    CreatePage,
    UpdatePage
}
