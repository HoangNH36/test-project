import React, { Component } from 'react';
import * as containers from '../containers'
class UpdatePage extends Component {
  render() {
    return (
      <div className="UpdatePage">
        <containers.FormUpdateContainer />
      </div>
    );
  }
}

export default UpdatePage;