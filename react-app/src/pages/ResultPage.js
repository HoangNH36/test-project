import React, { Component } from 'react';
import * as containers from '.././containers/index'
class ResultPage extends Component {
  render() {
    return (
      <div className="ResultPage">
        <containers.ResultContainer />
      </div>
    );
  }
}

export default ResultPage;