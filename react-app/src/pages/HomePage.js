import React, { Component } from 'react';
import * as containers from '.././containers/index'
class HomePage extends Component {
  render() {
    return (
      <div className="HomePage">
        <containers.TrucksContainer />
      </div>
    );
  }
}

export default HomePage;