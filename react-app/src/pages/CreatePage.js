import React, { Component } from 'react';
import * as containers from '../containers'
class CreatePage extends Component {
  render() {
    return (
      <div className="CreatePage">
        <containers.FormCreateContainer />
      </div>
    );
  }
}

export default CreatePage;